-- CS 448 Project 3 README --

- Group Members -
Joshua Selbo
Hunter Martin

- Test Output -
A copy of the test output is located in the file test-output.txt.

- Notes -
Any assumptions are explained in additions made to method comments.
The largest assumption is that made in HashJoin.java, copied here:

// Strategy: pre-process iterators to avoid possible delay spikes when calling getNext().
// Assumption: the set of joined Tuples fits into memory. This is the case with
// our given test cases, so I believe it is a valid assumption.
