package relop;

import global.RID;
import global.SearchKey;
import heap.HeapFile;
import index.BucketScan;
import relop.IndexScan;
import relop.FileScan;
import relop.HashTableDup;
import relop.Schema;
import index.HashIndex;

import java.util.List;
import java.util.ArrayList;

/**
 * Implements the hash-based join algorithm described in section 14.4.3 of the
 * textbook (3rd edition; see pages 463 to 464). HashIndex is used to partition
 * the tuples into buckets, and HashTableDup is used to store a partition in
 * memory during the matching phase.
 */
public class HashJoin extends Iterator {
  private Iterator leftIterator, rightIterator;
  private Integer leftJoinColumn, rightJoinColumn;

  // Holds BucketScan and backing HeapFile for each iterator.
  private ScanInfo leftScanInfo, rightScanInfo;
  private boolean isOpen;

  private List<Tuple> joinedTuples;
  private int joinedIndex;

  /**
   * Constructs a hash join, given the left and right iterators and which
   * columns to match (relative to their individual schemas).
   */
  public HashJoin(Iterator left, Iterator right, Integer lcol, Integer rcol) {
    this.schema = Schema.join(left.schema, right.schema);

    leftIterator = left;
    rightIterator = right;
    leftJoinColumn = lcol;
    rightJoinColumn = rcol;

    initialize();
  }

  /**
   * Gives a one-line explaination of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    System.out.printf("HashJoin Iterator {schema: %s, lcol: %s, rcol: %s}\n",
        this.schema, leftJoinColumn, rightJoinColumn);
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    leftIterator.restart();
    rightIterator.restart();

    initialize();
  }

  private void initialize() {
    // Build HashIndex for each file (partitioning phase).
    leftScanInfo = createIndexAndOpenBucketScan(leftIterator, leftJoinColumn);
    rightScanInfo = createIndexAndOpenBucketScan(rightIterator, rightJoinColumn);
    joinedTuples = new ArrayList<Tuple>();
    joinedIndex = 0;

    // Strategy: pre-process iterators to avoid possible delay spikes when calling getNext().
    // Assumption: the set of joined Tuples fits into memory. This is the case with
    // our given test cases, so I believe it is a valid assumption.
    processIterators();
    isOpen = true;
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    return isOpen;
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    leftIterator.close();
    rightIterator.close();

    isOpen = false;
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    checkOpen();

    return (joinedIndex < joinedTuples.size());
  }

  /**
   * Gets the next tuple in the iteration.
   * 
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    checkOpen();

    if (!hasNext()) {
      throw new IllegalStateException("No more Tuples");
    }

    Tuple ret = joinedTuples.get(joinedIndex);
    joinedIndex++;
    return ret;
  }

  /**
   * @throws IllegalStateException if scan has been closed
   */
  private void checkOpen() {
    if (!isOpen) {
      throw new IllegalStateException("HashJoin is closed");
    }
  }

  private FileScan copyAndOpenFileScan(Iterator iterator) {
    if (iterator instanceof FileScan) {
      // If iterator is a FileScan, it already has a backing HeapFile,
      // so there is no sense in copying another one.
      return (FileScan) iterator;
    }

    // Create temporary HeapFile
    HeapFile heapFile = new HeapFile(null);
    while (iterator.hasNext()) {
      Tuple next = iterator.getNext();
      heapFile.insertRecord(next.getData());
    }
    return new FileScan(iterator.schema, heapFile);
  }

  private ScanInfo createIndexAndOpenBucketScan(Iterator iterator, Integer joinCol) {
    if (iterator instanceof IndexScan) {
      // If iterator is an IndexScan, no need to rebuild the index.
      IndexScan indexedIterator = (IndexScan) iterator;
      HashIndex index = indexedIterator.getHashIndex();
      BucketScan scan = index.openScan();
      return new ScanInfo(scan, indexedIterator.getHeapFile());
    }

    // First, load tuples into a HeapFile.
    FileScan fileScan = copyAndOpenFileScan(iterator);

    // Populate temporary index.
    HashIndex index = new HashIndex(null);
    while (fileScan.hasNext()) {
      Tuple next = fileScan.getNext();
      Object field = next.getField(joinCol);
      SearchKey key = new SearchKey(field);

      index.insertEntry(key, fileScan.getLastRID());
    }
    fileScan.close();

    BucketScan scan = index.openScan();
    return new ScanInfo(scan, fileScan.getHeapFile());
  }

  private void processIterators() {
    BucketScan leftBucketScan = leftScanInfo.bucketScan;
    BucketScan rightBucketScan = rightScanInfo.bucketScan;

    // Probing phase.
    HashTableDup keyHash = new HashTableDup();
    while (leftBucketScan.hasNext()) {
      // Iterate through partition (all entries with same hash value) and add
      // entries to a hash table with the appropriate SearchKey.
      int currHash = leftBucketScan.getNextHash();
      do {
        RID next = leftBucketScan.getNext();
        byte[] recordData = leftScanInfo.heapFile.selectRecord(next);
        Tuple record = new Tuple(leftIterator.schema, recordData);
        SearchKey leftKey = new SearchKey(record.getField(leftJoinColumn));

        keyHash.add(leftKey, record);
      } while (leftBucketScan.hasNext() && leftBucketScan.getNextHash() == currHash);

      // Scan right iterator in same partition.
      while (rightBucketScan.hasNext() && rightBucketScan.getNextHash() == currHash) {
        RID next = rightBucketScan.getNext();
        byte[] recordData = rightScanInfo.heapFile.selectRecord(next);
        Tuple rightTuple = new Tuple(rightIterator.schema, recordData);
        Object rightField = rightTuple.getField(rightJoinColumn);
        SearchKey rightKey = new SearchKey(rightField);

        // Probe for tuples with same SearchKey.
        Tuple[] leftTuples = keyHash.getAll(rightKey);
        if (leftTuples != null) {
          // Iterate and check for exact matches.
          for (Tuple leftTuple : leftTuples) {
            Object leftField = leftTuple.getField(leftJoinColumn);
            if (leftField.equals(rightField)) {
              // We've made a match.
              Tuple joined = Tuple.join(leftTuple, rightTuple, this.schema);
              joinedTuples.add(joined);
            }
          }
        }
      }

      keyHash.clear();
    }
  }

  /**
   * Internal utility class which holds a reference to a BucketScan and
   * a reference to its backing HeapFile.
   */
  private class ScanInfo {
    BucketScan bucketScan;
    HeapFile heapFile;

    public ScanInfo(BucketScan bucketScan, HeapFile heapFile) {
      this.bucketScan = bucketScan;
      this.heapFile = heapFile;
    }
  }

} // public class HashJoin extends Iterator
