package relop;

import java.util.Arrays;

/**
 * The simplest of all join algorithms: nested loops (see textbook, 3rd edition,
 * section 14.4.1, page 454).
 */
public class SimpleJoin extends Iterator {
  private Iterator left;
  private Iterator right;
  private Predicate[] predicates;

  // Current leftTuple we are using to join with tuples in the right iterator
  private Tuple currLeftTuple;

  // Used for pre-fetching. Null indicates we are done iterating
  private Tuple nextTuple;

  /**
   * Constructs a join, given the left and right iterators and join predicates
   * (relative to the combined schema).
   */
  public SimpleJoin(Iterator left, Iterator right, Predicate... preds) {
    this.schema = Schema.join(left.schema, right.schema);

    this.left = left;
    this.right = right;
    predicates = preds;

    initialize();
  }

  /**
   * Gives a one-line explaination of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    for (int i = 0; i < depth; i++) {
      System.out.printf(" ");
    }
    System.out.printf("SimpleJoin {left iterator: ");
    left.explain(depth + 2);
    System.out.printf(", right iterator: ");
    right.explain(depth + 2);
    System.out.printf(", predicates: %s}\n", Arrays.toString(predicates));
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    left.restart();
    right.restart();

    initialize();
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    // Only necessary to check left, since both iterators have same closed status.
    return left.isOpen();
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    left.close();
    right.close();
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    return (nextTuple != null);
  }

  /**
   * Gets the next tuple in the iteration.
   * 
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    if (nextTuple == null) {
      throw new IllegalStateException("No more tuples");
    }

    Tuple ret = nextTuple;
    prefetchNextTuple();
    return ret;
  }

  private void initialize() {
    // If either left or right iterator is empty, we have no tuples to join
    if (!left.hasNext() || !right.hasNext()) {
      nextTuple = null;
      return;
    }

    // Fetch first left tuple
    currLeftTuple = left.getNext();

    prefetchNextTuple();
  }

  /**
   * Precondition: both left and right iterators are non-empty.
   */
  private void prefetchNextTuple() {
    boolean searching = true;
    while (searching) {
      // Done iterating
      if (currLeftTuple == null) {
        nextTuple = null;
        break;
      }

      // Guaranteed by later check (right.hasNext()) to succeed.
      Tuple rightTuple = right.getNext();
      nextTuple = Tuple.join(currLeftTuple, rightTuple, this.schema);

      // If matches any prediate, stop searching
      for (Predicate pred : predicates) {
        if (pred.evaluate(nextTuple)) {
          searching = false;
          break;
        }
      }

      // Prepare for next call
      if (!right.hasNext()) {
        // We have finished iterating inside iterator. Increment outside and restart inside.
        if (left.hasNext()) {
          currLeftTuple = left.getNext();
          right.restart();
        } else {
          // We are done iterating entirely.
          currLeftTuple = null;
        }
      }
    }
  }

} // public class SimpleJoin extends Iterator
