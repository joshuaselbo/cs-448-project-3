package relop;

import java.util.Arrays;

/**
 * The projection operator extracts columns from a relation; unlike in
 * relational algebra, this operator does NOT eliminate duplicate tuples.
 */
public class Projection extends Iterator {
  private Iterator provider;
  private Integer[] fields;

  /**
   * Constructs a projection, given the underlying iterator and field numbers.
   * 
   * Assumptions:
   * - fields is non-null
   * - each object in fields is a non-null Integer
   * - each Integer in fields is in range [0, fields.length)
   * - each Integer in fields is unique
   */
  public Projection(Iterator iter, Integer... fields) {
    this.schema = projectSchema(iter.schema, fields);
    provider = iter;
    this.fields = fields;
  }

  /**
   * Gives a one-line explaination of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    for (int i = 0; i < depth; i++) {
      System.out.printf(" ");
    }
    System.out.printf("Projection {iterator: ");
    provider.explain(depth + 2);
    System.out.printf(", fields: %s}\n", Arrays.toString(fields));
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    provider.restart();
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    return provider.isOpen();
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    provider.close();
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    return provider.hasNext();
  }

  /**
   * Gets the next tuple in the iteration.
   * 
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    Tuple rawNext = provider.getNext();

    Object[] projFieldValues = new Object[fields.length];
    int index = 0;
    for (Integer fldno : fields) {
      projFieldValues[index] = rawNext.getField(fldno);
      index++;
    }

    Tuple projNext = new Tuple(this.schema);
    projNext.setAllFields(projFieldValues);
    return projNext;
  }

  /**
   * Returns a schema with the given fields projected from the given schema
   */
  private Schema projectSchema(Schema schema, Integer[] fields) {
    Schema projSchema = new Schema(fields.length);

    int index = 0;
    for (Integer fldno : fields) {
      projSchema.initField(index, schema, fldno);
      index++;
    }

    return projSchema;
  }

} // public class Projection extends Iterator
