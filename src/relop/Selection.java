package relop;

import java.util.Arrays;

/**
 * The selection operator specifies which tuples to retain under a condition; in
 * Minibase, this condition is simply a set of independent predicates logically
 * connected by OR operators.
 */
public class Selection extends Iterator {
  private Iterator provider;
  private Predicate[] predicates;

  // Used for pre-fetching the next tuple. If null, no next tuple.
  private Tuple nextTuple;

  /**
   * Constructs a selection, given the underlying iterator and predicates.
   */
  public Selection(Iterator iter, Predicate... preds) {
    this.schema = iter.schema;
    provider = iter;
    predicates = preds;

    prefetchNextTuple();
  }

  /**
   * Gives a one-line explaination of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    for (int i = 0; i < depth; i++) {
      System.out.printf(" ");
    }
    System.out.printf("Selection {iterator: ");
    provider.explain(depth + 2);
    System.out.printf(", predicates: %s}\n", Arrays.toString(predicates));
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    provider.restart();

    prefetchNextTuple();
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    return provider.isOpen();
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    provider.close();
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    checkOpen();
    
    return (nextTuple != null);
  }

  /**
   * Gets the next tuple in the iteration.
   * 
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    checkOpen();

    if (nextTuple == null) {
      throw new IllegalStateException("No more tuples");
    }

    Tuple ret = nextTuple;
    prefetchNextTuple();
    return ret;
  }

  private void prefetchNextTuple() {
    Tuple nextMatching = null;
    while (provider.hasNext()) {
      Tuple next = provider.getNext();

      // Check if next provided Tuple matches any predicate
      boolean matchesAnyPredicate = false;
      for (Predicate pred : predicates) {
        if (pred.evaluate(next)) {
          matchesAnyPredicate = true;
          break;
        }
      }

      // If so, stop iterating
      if (matchesAnyPredicate) {
        nextMatching = next;
        break;
      }
    }

    // Will be null if no next matching predicate was found
    nextTuple = nextMatching;
  }

  /**
   * @throws IllegalStateException if scan has been closed
   */
  private void checkOpen() {
    if (!isOpen()) {
      throw new IllegalStateException("Selection is closed");
    }
  }

} // public class Selection extends Iterator
