package relop;

import global.RID;
import heap.HeapFile;
import heap.HeapScan;

/**
 * Wrapper for heap file scan, the most basic access method. This "iterator"
 * version takes schema into consideration and generates real tuples.
 */
public class FileScan extends Iterator {
  private HeapFile file;
  private HeapScan scan;
  private RID lastRID;
  private boolean isOpen;

  /**
   * Constructs a file scan, given the schema and heap file.
   */
  public FileScan(Schema schema, HeapFile file) {
    this.schema = schema;
    this.file = file;

    initialize();
  }

  /**
   * Gives a one-line explaination of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    for (int i = 0; i < depth; i++) {
      System.out.printf(" ");
    }
    System.out.printf("FileScan Iterator {schema: %s, last RID: %d, %d}\n",
        schema, lastRID.pageno, lastRID.slotno);
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    if (isOpen) {
      close();
    }

    initialize();
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    return isOpen;
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    scan.close();
    isOpen = false;
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    checkOpen();
    
    return scan.hasNext();
  }

  /**
   * Gets the next tuple in the iteration.
   * 
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    checkOpen();

    // If this is the first record requested, allow scan.getNext(RID) to write to lastRID.
    if (lastRID == null) {
      lastRID = new RID();
    }

    byte[] record = scan.getNext(lastRID);
    return new Tuple(this.schema, record);
  }

  /**
   * Gets the RID of the last tuple returned.
   *
   * Assumption: This method is valid to call after the close() method is called.
   * Assumption: Returns null before getNext() has been called for the first time.
   */
  public RID getLastRID() {
    return lastRID;
  }

  /**
   * Retrieve backing HeapFile for this FileScan.
   */
  protected HeapFile getHeapFile() {
    return file;
  }

  /**
   * Internal init method.
   */
  private void initialize() {
    scan = file.openScan();
    lastRID = null;
    isOpen = true;
  }

  /**
   * @throws IllegalStateException if scan has been closed
   */
  private void checkOpen() {
    if (!isOpen) {
      throw new IllegalStateException("FileScan is closed");
    }
  }

} // public class FileScan extends Iterator
