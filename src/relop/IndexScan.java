package relop;

import global.RID;
import global.SearchKey;
import heap.HeapFile;
import index.BucketScan;
import index.HashIndex;

/**
 * Wrapper for bucket scan, an index access method.
 */
public class IndexScan extends Iterator {
  private HashIndex index;
  private HeapFile file;

  private BucketScan scan;
  private boolean isOpen;

  /**
   * Constructs an index scan, given the hash index and schema.
   */
  public IndexScan(Schema schema, HashIndex index, HeapFile file) {
    this.schema = schema;
    this.index = index;
    this.file = file;

    initialize();
  }

  /**
   * Gives a one-line explaination of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    for (int i = 0; i < depth; i++) {
      System.out.printf(" ");
    }
    System.out.printf("IndexScan Iterator {schema: %s, index: %s}\n",
        this.schema, index);
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    if (isOpen) {
      close();
    }

    initialize();
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    return isOpen;
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    scan.close();
    isOpen = false;
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    checkOpen();

    return scan.hasNext();
  }

  /**
   * Gets the next tuple in the iteration.
   * 
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    checkOpen();

    RID recordID = scan.getNext();
    byte[] record = file.selectRecord(recordID);
    return new Tuple(this.schema, record);
  }

  /**
   * Gets the key of the last tuple returned.
   *
   * Assumption: This method is valid to call after the close() method is called.
   */
  public SearchKey getLastKey() {
    return scan.getLastKey();
  }

  /**
   * Returns the hash value for the bucket containing the next tuple, or maximum
   * number of buckets if none.
   */
  public int getNextHash() {
    checkOpen();

    return scan.getNextHash();
  }

  /**
   * Returns the backing HashIndex used for scanning.
   */
  protected HashIndex getHashIndex() {
    return index;
  }

  /**
   * Returns the backing HeapFile used for scanning.
   */
  protected HeapFile getHeapFile() {
    return file;
  }

  /**
   * Internal init method.
   */
  private void initialize() {
    scan = index.openScan();
    isOpen = true;
  }

  /**
   * @throws IllegalStateException if scan has been closed
   */
  private void checkOpen() {
    if (!isOpen) {
      throw new IllegalStateException("IndexScan is closed");
    }
  }

} // public class IndexScan extends Iterator
