package relop;

import global.RID;
import global.SearchKey;
import heap.HeapFile;
import index.HashIndex;
import index.HashScan;

/**
 * Wrapper for hash scan, an index access method.
 */
public class KeyScan extends Iterator {
  private HashIndex index;
  private SearchKey key;
  private HeapFile file;

  private HashScan scan;
  private boolean isOpen;

  /**
   * Constructs an index scan, given the hash index and schema.
   */
  public KeyScan(Schema schema, HashIndex index, SearchKey key, HeapFile file) {
    this.schema = schema;
    this.index = index;
    this.key = key;
    this.file = file;

    initialize();
  }

  /**
   * Gives a one-line explaination of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    for (int i = 0; i < depth; i++) {
      System.out.printf(" ");
    }
    System.out.printf("KeyScan Iterator {schema: %s, index: %s, key: %s}\n",
        this.schema, index, key);
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    if (isOpen) {
      close();
    }

    initialize();
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    return isOpen;
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    scan.close();
    isOpen = false;
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    checkOpen();

    return scan.hasNext();
  }

  /**
   * Gets the next tuple in the iteration.
   * 
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    checkOpen();

    RID recordID = scan.getNext();
    byte[] record = file.selectRecord(recordID);
    return new Tuple(this.schema, record);
  }

  /**
   * Internal init method.
   */
  private void initialize() {
    scan = index.openScan(key);
    isOpen = true;
  }

  /**
   * @throws IllegalStateException if scan has been closed
   */
  private void checkOpen() {
    if (!isOpen) {
      throw new IllegalStateException("KeyScan is closed");
    }
  }

} // public class KeyScan extends Iterator
